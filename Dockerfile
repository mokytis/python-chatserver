FROM python:3.8.0-slim

RUN pip install poetry
WORKDIR /tmp/chatserver

COPY pyproject.toml poetry.lock ./
RUN cd /tmp/chatserver && poetry install --no-root --no-dev

COPY . .
RUN poetry install --no-dev --no-interaction --no-ansi

CMD poetry run chatserver

VOLUME /certs
EXPOSE 7878/tcp
